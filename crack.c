#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    int i = 0;
    while(guess[i] != '\0')
        i++;

    char *hashguess = md5(guess, i-1);
    // Compare the two hashes
    strtok(hash, "\n");
    if (strcmp(hashguess, hash) == 0)
        return 1;
    else return 0;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    // After I am done constructing the array I need to add a null character
    // on the end of it to show the end
    
    FILE *f = fopen(filename, "r"); //open file I passed in
    if(!f) //check if you can open da file
        {
            printf("Couldn't open the read file\n");
            exit(1);
        }
    
    int totalsize = 0;
    int i = 0;
    char **m;
    m = malloc(200 * sizeof(char *));
    char temp[1000];
    while(fgets(temp, 1000, f) != NULL)
    {
        int len = strlen(temp); // Get length of lines
        m[i] = malloc(len * sizeof(char) + 1); //add one to count for null character
        strcpy(m[i], temp);
        i++;
        if (i == totalsize) //walked off the end of teh array
        {
            totalsize += 200;
            char ** newm = realloc(m, totalsize * sizeof(char *));
            if (newm == NULL)
                printf("Could not extend array\n");
                
            else if (newm != m)
                m = newm;
        }
    }
    fclose(f);
    m[i] = malloc(sizeof(char));
    m[i] = '\0';
    return m;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    
    int p = tryguess(hashes[1], dict[1]);
    printf("p is = %d\n", p);
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int array_length1 = 0;
    while(hashes[array_length1] != '\0')
        array_length1++;
    
    int array_length2 = 0;
    while(dict[array_length2] != '\0')
        array_length2++;
    
    
    for(int i = 0; i < array_length1; i++)
    {
        for(int x = 0; x < array_length2; x++)
        {
            if(tryguess(hashes[i], dict[x]) == 1)
                printf("%s\n", dict[x]);
        }
    }
}
